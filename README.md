## Stopped supporting this project!, sorry
# Photo from the project
![Linux Version](https://user-images.githubusercontent.com/28893833/50518641-a1674e00-0aae-11e9-872a-cf006f0bb7c9.png)
## ----------------------------------------

| #  | Name | Link |
| ------------- | ------------- | ------------- |
| 1  | What's New  | https://gitlab.com/EsmailELBoBDev2/water-drink-reminder#--1  |
| 2  | How To Edit Codes| https://gitlab.com/EsmailELBoBDev2/water-drink-reminder#--2  |
| 3  | Downloads  | https://gitlab.com/EsmailELBoBDev2/water-drink-reminder#--3  |
| 4  | License  | https://gitlab.com/EsmailELBoBDev2/water-drink-reminder/blob/master/COPYING |
| 5  | My Other Projects  | https://gitlab.com/EsmailELBoBDev2/water-drink-reminder#--6  |
| 6  | Thx!  | https://gitlab.com/EsmailELBoBDev2/water-drink-reminder#--7  |
## ----------------------------------------
# What's New ?                      

## (For Users): 
1. **Added Notifications**

2. **Fixed Freezed Alram Problem**

## (For Devs): 
1. **Improved Codes**
## ----------------------------------------
# How To Edit Codes

1. **Clone Project Files**: `git clone https://gitlab.com/EsmailELBoBDev2/water-drink-reminder.git` (*You Not Need*: **README.md** *&* **COPYING** *files*)

2. **Download Code Editor** (*Skip This, If You Already Have One*): *https://yerl.org/5PJau*

3. **Extract Files** (*On Dektop As Example*)

4. **Open Your Text Editor & Drag & Drop My Project's Files** (*Drag & Drop* **Index.html** *As Example*)
## ----------------------------------------
# Downloads

**Clone Project Files**(*Source Code*): `git clone https://gitlab.com/EsmailELBoBDev2/water-drink-reminder.git`

**The WDR App** (*Water Drink Reminder App*)(*.deb*): ***https://yerl.org/orBOP*** (*It's For Linux Users, BUT You Can Run It On Win/Mac By Downloading Source Code Then Run `npm start` CMD, [See/Click Me If You Stuck!](https://youtu.be/_3k-Lf16Su8))*

**SublimeText** (*Code Editor*): ***https://yerl.org/hHgeb***

**Visual Studio Code** (*Code Editor*): ***https://yerl.org/AHuvV***

**Atom** (*Code Editor*): ***https://yerl.org/5PJau***
## ----------------------------------------
# My Other Projects

**https://github.com/EsmailELBoBDev2?tab=repositories** *,* **https://gitlab.com/users/EsmailELBoBDev2/projects**
## ----------------------------------------

# Thx For Using My Projects :smiley:



