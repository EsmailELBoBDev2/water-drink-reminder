let alert = new Audio("alert.mp3");
const ONE_HOUR = 1000 * 60 * 60;
let countDownDate;
let running;
let remainingTime;
let update;
initCountdown();

function startInterval() {
  return setInterval(function() {
    let now = new Date().getTime();
    remainingTime = countDownDate - now;

    let hours = Math.floor((remainingTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    let minutes = Math.floor((remainingTime % (1000 * 60 * 60)) / (1000 * 60));
    let seconds = Math.floor((remainingTime % (1000 * 60)) / 1000);
    let millis = Math.floor((remainingTime % 1000));
    document.getElementById("timer").innerHTML = hours + ":" + minutes + ":" + seconds + "." + millis;

    if (remainingTime < 0) {
      let waterNotification = new Notification("It's Rewind Time", {
        body: "I Meant, It's Water Drinking Time",
        icon: "logo.png"
      });
      waterNotification.onshow = function() {
        alert.loop = true;
        alert.play();
      };
      document.getElementById("timer").innerHTML = "It's Water Drinking Time!";
      clearInterval(update);
    }
  }, 10);
}

function stopTimer() {
  if (running) {
    let now = new Date().getTime();
    remainingTime = countDownDate - now;
    running = false;
    clearInterval(update);
  }
}

function startTimer() {
  if (!running) {
    countDownDate = new Date().getTime() + remainingTime;
    update = startInterval();
    running = true;
  }
}

function initCountdown() {
  countDownDate = new Date().getTime() + ONE_HOUR;
  update = startInterval();
  running = true;
}

/*function restart() {
    initCountdown();
    debugger;
}*/

function restart() {
  location.reload();
}

function testAlert() {
  alert.loop = true;
  alert.play();


}

function testNotifications() {
  let waterNotification = new Notification("It's Rewind Time", {
    body: "I Meant, It's Water Drinking Time",
    icon: "logo.png"
  });
  waterNotification.onshow = function() {
    alert.play();
  };
}

function stopAlert() {
  alert.pause();
}
