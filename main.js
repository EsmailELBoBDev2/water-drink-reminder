const electron = require('electron');
const app = electron.app;

const path = require('path');
const url = require('url');

const BrowserWindow = electron.BrowserWindow;

let mainWindow;
app.on('ready',function(){
	mainWindow = new BrowserWindow({width: 893, height: 346});
	//mainWindow.loadURL('https://github.com');
	 mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
		protocol: 'file:',
		slashes: true
		
	}));

});
